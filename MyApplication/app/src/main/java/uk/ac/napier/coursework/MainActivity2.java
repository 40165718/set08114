package uk.ac.napier.coursework;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }

    @Override
     public boolean onCreateOptionsMenu(Menu menu)  {
        //inflate them menu; this adds items to the actionbar if it is pressed
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        //handle the action bar item click here the action bar will automatically handle the clicks on the home/up button.
        // so as long as you specify a parent activity in AndroidManifest.xm
        int id = item.getItemId();
        if (id== R.id.action_settings){
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
