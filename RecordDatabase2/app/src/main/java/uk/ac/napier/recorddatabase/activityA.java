package uk.ac.napier.recorddatabase;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.util.Log;
import android.view.View.OnClickListener;



/**
 * Created by 40165718 on 27/03/2016.
 */
public class activityA extends AppCompatActivity{
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a);
        Toast.makeText(getBaseContext(),"In activity A", Toast.LENGTH_LONG).show();
        Log.i("napier.ac.uk", "Gone to the Database page");
        Button button4 = (Button) findViewById(R.id.button4);

        button4.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Intent activityGenre = new Intent(activityA.this, activityGenre.class);
                startActivity(activityGenre);
            }
        });

    }


}
