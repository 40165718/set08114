package uk.ac.napier.recorddatabase;

/**
 * Created by Alex on 01/04/2016.
 */
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.util.Log;
import android.view.View.OnClickListener;
public class activityGenre extends AppCompatActivity{
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genre);
        Toast.makeText(getBaseContext(),"In activity A", Toast.LENGTH_LONG).show();
        Log.i("napier.ac.uk", "Gone to the genres page");
        Intent genre = new Intent(activityGenre.this, genre.class);
        startActivity(genre);
    }
}