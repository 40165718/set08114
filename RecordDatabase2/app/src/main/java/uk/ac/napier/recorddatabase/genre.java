package uk.ac.napier.recorddatabase;
/**
 * 40165718
 * App Dev Coursework
 * Genre.java
 */
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class genre extends AppCompatActivity {
    private TextView idView;
    private EditText albumBox;
    private EditText quantityBox;
    private EditText artistBox;
    private EditText genreBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_genre);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        idView = (TextView) findViewById(R.id.albumID);
        albumBox = (EditText) findViewById(R.id.albumName);
        quantityBox = (EditText) findViewById(R.id.numOfTracks);
        artistBox = (EditText) findViewById(R.id.artistName);
        genreBox = (EditText) findViewById(R.id.albumGenre);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    public void newAlbum(View view) {
        myDBHandler dbHandler = new myDBHandler(this, null, null, 1);

        int quantity = Integer.parseInt(quantityBox.getText().toString());

        albuminfo album = new albuminfo(albumBox.getText().toString(), quantity, artistBox.getText().toString(), genreBox.getText().toString());

        dbHandler.addAlbum(album);
        albumBox.setText("");
        quantityBox.setText("");
        genreBox.setText("");
        artistBox.setText("");
    }

    public void lookupItem (View view){
        myDBHandler dbHandler = new myDBHandler(this, null, null, 1);

        albuminfo album = dbHandler.findProduct(albumBox.getText().toString());

        if (album != null){
            idView.setText(String.valueOf(album.getID()));

            quantityBox.setText(String.valueOf(album.getNumofTracks()));
            artistBox.setText(String.valueOf(album.getArtistName()));
            genreBox.setText(String.valueOf(album.getGenre()));
        } else {
            idView.setText("No Match Found");
        }
    }
    public void findAlbum(View view){
        myDBHandler dbHandler = new myDBHandler(this, null, null, 1);
        albuminfo album = dbHandler.findAlbum(artistBox.getText().toString());
        if (album != null) {
            idView.setText(String.valueOf(album.getID()));

            quantityBox.setText(String.valueOf(album.getNumofTracks()));
            albumBox.setText(String.valueOf(album.getAlbumName()));
            genreBox.setText(String.valueOf(album.getGenre()));
        } else {
            idView.setText("No Match Found");
        }
    }

    public void deleteItem(View view){
        myDBHandler dbHandler = new myDBHandler(this, null, null, 1);

        boolean result = dbHandler.deleteAlbum(albumBox.getText().toString());

        if (result)
        {
            idView.setText("Record Deleted");
            albumBox.setText("");
            quantityBox.setText("");
            artistBox.setText("");
            genreBox.setText("");
        }
        else idView.setText("No Match Found");

        }

    public void clearItems(View view){
        //Method that clears all fo the input sections.
        albumBox.setText("");
        quantityBox.setText("");
        artistBox.setText("");
        genreBox.setText("");
    }
    }
