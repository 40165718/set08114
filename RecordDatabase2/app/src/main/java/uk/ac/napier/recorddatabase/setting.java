package uk.ac.napier.recorddatabase;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.TextView;

public class setting extends AppCompatActivity {
    public boolean blackBack = false;
    public boolean whiteBack = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        Button button = (Button) findViewById(R.id.button15);
        button.setOnClickListener(new OnClickListener(){
            @Override
        public void onClick(View view){
            RelativeLayout background = (RelativeLayout) findViewById(R.id.backgroundId);
            background.setBackgroundColor(Color.BLACK);
            blackBack = true;
            whiteBack = false;
        }});

        Button button1 = (Button) findViewById(R.id.button16);
        button1.setOnClickListener(new OnClickListener(){
            public void onClick(View view){
                RelativeLayout background = (RelativeLayout) findViewById(R.id.backgroundId);
                background.setBackgroundColor(Color.WHITE);
                blackBack = true;
                whiteBack = false;
            }});

       final TextView tv1 = (TextView) findViewById(R.id.textView6);
       final TextView tv2 = (TextView) findViewById(R.id.textView7);

        Button button2 = (Button) findViewById(R.id.button6);
        Button button3 = (Button) findViewById(R.id.button10);
        Button button4 = (Button) findViewById(R.id.button11);
        Button button5 = (Button) findViewById(R.id.button12);
        Button button6 = (Button) findViewById(R.id.button13);
        Button button7 = (Button) findViewById(R.id.button14);

        button2.setOnClickListener(new OnClickListener(){
            public void onClick(View view){
                tv1.setTextColor(Color.BLACK);
                tv2.setTextColor(Color.BLACK);
            }});
        button3.setOnClickListener(new OnClickListener(){
            public void onClick(View view){
                tv1.setTextColor(Color.WHITE);
                tv2.setTextColor(Color.WHITE);
            }});
        button4.setOnClickListener(new OnClickListener(){
            public void onClick(View view){
                tv1.setTextColor(Color.RED);
                tv2.setTextColor(Color.RED );
            }});
        button5.setOnClickListener(new OnClickListener(){
            public void onClick(View view){
                tv1.setTextColor(Color.BLUE);
                tv2.setTextColor(Color.BLUE);
            }});
        button6.setOnClickListener(new OnClickListener(){
            public void onClick(View view){
                tv1.setTextColor(Color.GREEN);
                tv2.setTextColor(Color.GREEN);
            }});
        button7.setOnClickListener(new OnClickListener(){
            public void onClick(View view){
                tv1.setTextColor(Color.YELLOW);
                tv2.setTextColor(Color.YELLOW);
            }});

    }


}
