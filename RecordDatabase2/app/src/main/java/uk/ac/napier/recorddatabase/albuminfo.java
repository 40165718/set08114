package uk.ac.napier.recorddatabase;

/**
 * 40165718
 * App Dev Coursework
 * RecordDatabase App
 */

/**
 * this class contains all the private data members for the internal storage from the database
 * entries and set those values.
 */
public class albuminfo {
    private int _idnum;
    private String _albumname;
    private int _numoftracks;
    private String _artistName;
    private String _genre;

    public albuminfo(){

    }

    public albuminfo(int idnum, String albumname, int numoftracks, String artistName, String AlbumGenre){
        this._idnum = idnum;
        this._albumname = albumname;
        this._numoftracks = numoftracks;
        this._artistName = artistName;
        this._genre = AlbumGenre;
    }

    public albuminfo(String albumname, int numoftracks, String artistName, String AlbumGenre){
        this._albumname =albumname;
        this._numoftracks =numoftracks;
        this._artistName = artistName;
        this._genre = AlbumGenre;
    }
    public void setID(int idnum){
        this._idnum=idnum;
    }

    public int getID(){
        return this._idnum;
    }

    public void setalbumname(String albumname){
        this._albumname = albumname;
    }

    public String getAlbumName(){
        return this._albumname;
    }

    public void setNumofTracks(int numoftracks){
        this._numoftracks=numoftracks;
    }

    public int getNumofTracks(){
        return this._numoftracks;
    }
    public void setArtistName(String artistName){
        this._artistName =  artistName;
    }

    public String getArtistName(){
        return this._artistName;
    }

    public void setGenre(String AlbumGenre){
        this._genre =AlbumGenre;
    }

    public String getGenre(){
        return this._genre;
    }
}
