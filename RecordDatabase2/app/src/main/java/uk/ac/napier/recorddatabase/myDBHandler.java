package uk.ac.napier.recorddatabase;

import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.content.Context;
import android.content.ContentValues;
import android.database.Cursor;

import java.util.ArrayList;


/**
 * 401651718
 * App Dev Coursework
 * Record Database
 * */
public class myDBHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 5;
    private static final String DATABASE_NAME = "albuminfo.db";
    private static final String TABLE_ALBUMS = "albums";
    // List of all the different columns in the table.
    public static final String COLUMN_ID= "_idnum";
    public static final String COLUMN_ALBUMNAME = "albumname";
    public static final String COLUMN_NUMOFTRACKS = "numoftracks";
    public static final String COLUMN_ARTISTNAME = "artistname";
    public static final String COLUMN_ALBUMGENRE = "albumgenre";

    public myDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version)
    {
        super(context,DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        //Part that makes the actual table itself.
        String CREATE_ALBUMS_TABLE= "CREATE TABLE " + TABLE_ALBUMS + "("+ COLUMN_ID +
                " INTEGER PRIMARY KEY,"+ COLUMN_ALBUMNAME +" TEXT," + COLUMN_NUMOFTRACKS +
                " INTEGER, " + COLUMN_ARTISTNAME + " TEXT," + COLUMN_ALBUMGENRE + " TEXT" + ")";

        db.execSQL(CREATE_ALBUMS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ALBUMS);
        onCreate(db);
    }

    public void addAlbum(albuminfo albums){
        //The method that allows you to add new items to the database
        ContentValues values = new ContentValues();
        values.put(COLUMN_ALBUMNAME, albums.getAlbumName()); //Adds the album name
        values.put(COLUMN_NUMOFTRACKS, albums.getNumofTracks()); //Adds the number of tracks
        values.put(COLUMN_ALBUMGENRE, albums.getGenre()); //Adds the genre of the album
        values.put(COLUMN_ARTISTNAME, albums.getArtistName()); //Adds the Name of the artist

        SQLiteDatabase db = this.getWritableDatabase();

        db.insert(TABLE_ALBUMS, null, values); //Inserts these all into the Database
        db.close();
    }

    public albuminfo findProduct(String albumname){
        //Method that allows you to search for an album
     String query = "Select * FROM " + TABLE_ALBUMS + " WHERE " + COLUMN_ALBUMNAME + "= \"" + albumname + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        albuminfo album = new albuminfo();

        if (cursor.moveToFirst()){
            cursor.moveToFirst();
            album.setID(Integer.parseInt(cursor.getString(0))); //Puts the ID onto the page
            album.setalbumname(cursor.getString(1)); //Puts the album name onto the page
            album.setNumofTracks(Integer.parseInt(cursor.getString(2)));//Puts the number of tracks onto the page
            album.setArtistName(cursor.getString(3)); // Puts the name of the artist on the page
            album.setGenre(cursor.getString(4));//Puts the genre on the screen.
            cursor.close();
        } else {
            album = null;
        }
        db.close();
        return album;
    }
    public albuminfo findAlbum(String artistname){
        String query = "Select * FROM " + TABLE_ALBUMS + " WHERE " + COLUMN_ARTISTNAME + "= \"" +artistname + "\"";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);
        albuminfo album = new albuminfo();

        if (cursor.moveToFirst()){
            cursor.moveToFirst();
            album.setID(Integer.parseInt(cursor.getString(0))); //Puts the ID onto the page
            album.setalbumname(cursor.getString(1)); //Puts the album name onto the page
            album.setNumofTracks(Integer.parseInt(cursor.getString(2)));//Puts the number of tracks onto the page
            album.setArtistName(cursor.getString(3)); // Puts the name of the artist on the page
            album.setGenre(cursor.getString(4));//Puts the genre on the screen.
            cursor.close();
        } else {
            album = null;
        }
        db.close();
        return album;
    }

    public boolean deleteAlbum( String albumname){
        //The method for deleting a database record.
        boolean result = false;
        String query = "Select * FROM " + TABLE_ALBUMS + " WHERE " + COLUMN_ALBUMNAME + "= \"" + albumname + "\"";
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);

        albuminfo album = new albuminfo();

        if (cursor.moveToFirst()){
            album.setID(Integer.parseInt(cursor.getString(0)));
            db.delete(TABLE_ALBUMS, COLUMN_ID + " = ?", new String[]{String.valueOf(album.getID())});
            cursor.close();
            result = true;
        }
        db.close();
        return result;
    }
}
